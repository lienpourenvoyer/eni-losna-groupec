package bll;

import bo.Categories;
import dal.CategoriesDAO;
import dal.DAOFactory;

import java.util.List;

public class CategoriesManager {
    private CategoriesDAO categoriesDAO;

    public CategoriesManager(){
        this.categoriesDAO = DAOFactory.getCategoriesDAO();
    }

    public List<Categories> afficherLesCategories(){ return this.categoriesDAO.selectAll(); }

}
