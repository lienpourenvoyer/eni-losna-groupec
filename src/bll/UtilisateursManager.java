package bll;

import bo.Utilisateurs;
import dal.DAOFactory;
import dal.UtilisateursDAO;

public class UtilisateursManager {
    private UtilisateursDAO UDAO;
    public UtilisateursManager(){this.UDAO = DAOFactory.getUtilisateursDAO();}

    public void insert(Utilisateurs user){this.UDAO.insert(user);}
    public void update(Utilisateurs user, Integer id){this.UDAO.update(user, id);}
    public void delete(Integer id){this.UDAO.delete(id);}
    public int login(String id, String pswd){return this.UDAO.login(id, pswd);}
    public Utilisateurs find(String id){ return this.UDAO.find(id);}
}
