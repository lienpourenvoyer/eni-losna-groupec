package bll;

import bo.Retraits;
import dal.DAOFactory;
import dal.RetraitsDAO;

public class RetraitsManager {
    private RetraitsDAO RDAO;
    public RetraitsManager() { this.RDAO = DAOFactory.getRetraitsDAO();}

    public void insert(Retraits rt){this.RDAO.insert(rt);}
    public Retraits selectID(int id_article){return this.RDAO.selectID(id_article);}
    public void update(Retraits rt){this.RDAO.update(rt);}
}
