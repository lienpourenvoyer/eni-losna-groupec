package bll;

import bo.ArticlesVendus;
import dal.ArticlesVendusDAO;
import dal.DAOFactory;

import java.util.List;

public class ArticlesVendusManager {
    private ArticlesVendusDAO articlesVendusDAO;

    public ArticlesVendusManager(){this.articlesVendusDAO = DAOFactory.getArticlesVendusDAO();}

    public List<ArticlesVendus> afficherLesArticles(){
        return this.articlesVendusDAO.selectAll();
    }

    public void insertVente(ArticlesVendus av){this.articlesVendusDAO.insertVente(av);}

    public ArticlesVendus selectID(int id){return this.articlesVendusDAO.selectID(id);}

    public void updateVente(ArticlesVendus av){this.articlesVendusDAO.updateVente(av);}


    public void deleteVente(Integer id){this.articlesVendusDAO.deleteVente(id);}

    public void updatePrix(int id_art, int prix){this.articlesVendusDAO.updatePrix(id_art, prix);}


    public List<ArticlesVendus> afficherArticleByCategorie(int no_categorie, String nomArticle) { return this.articlesVendusDAO.selectByCategorie(no_categorie, nomArticle); }
}
