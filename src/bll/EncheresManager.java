package bll;

import bo.Encheres;
import dal.DAOFactory;
import dal.EncheresDAO;

import java.time.LocalDate;
import java.util.List;

public class EncheresManager {
    private EncheresDAO EDAO;
    public EncheresManager() {this.EDAO = DAOFactory.getEnchereDAO();}

    public void insert(Encheres en){this.EDAO.insert(en);}
    public List<String> selectID(int id){return this.EDAO.selectID(id);}
    public void encherir(int id_art, int id_user, int montant, LocalDate date){this.EDAO.encherir(id_art, id_user, montant, date);}
}
