package ihm;

import bo.Utilisateurs;
import bll.UtilisateursManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletSignin")
public class ServletSignin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher rd;

        String pseudo = request.getParameter("inputPseudo");
        String nom    = request.getParameter("inputNom");
        String prenom = request.getParameter("inputPrenom");
        String mail   = request.getParameter("inputMail");
        String tel    = request.getParameter("inputTelephone");
        String ville  = request.getParameter("inputVille");
        String cp     = request.getParameter("inputCP");
        String rue    = request.getParameter("inputRue");
        String psw    = request.getParameter("inputPassword");
        String conf   = request.getParameter("inputPasswordConf");

        //TO-DO vérifier que les utilisateurs ont des noms de comptes différents
        if(psw.equals(conf)){
            UtilisateursManager um = new UtilisateursManager();
            Utilisateurs user = new Utilisateurs(pseudo, nom, prenom, mail, tel, rue, cp, ville, psw);
            um.insert(user);
            session.setAttribute("active_user", user);
            response.sendRedirect(request.getContextPath());
        }else{
            request.setAttribute("pswd", true);
            rd = request.getRequestDispatcher("/WEB-INF/signin.jsp");
            rd.forward(request,response);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/signin.jsp");
        rd.forward(request,response);
    }
}
