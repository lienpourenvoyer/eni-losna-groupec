package ihm;

import bll.ArticlesVendusManager;
import bll.EncheresManager;
import bll.UtilisateursManager;
import bo.ArticlesVendus;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(name = "ServletEnchere")
public class ServletEnchere extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        EncheresManager em = new EncheresManager();
        ArticlesVendusManager avm = new ArticlesVendusManager();
        UtilisateursManager um = new UtilisateursManager();

        Utilisateurs au = (Utilisateurs) session.getAttribute("active_user");

        int propal  = Integer.parseInt(request.getParameter("proposition"));
        int article = Integer.parseInt(request.getParameter("article"));

        if(au == null){
            response.sendRedirect(request.getContextPath()+"/login");
        }else{
            if(au.getCredit() >= propal){
                em.encherir(article, au.getNo_utilisateur(), propal, LocalDate.now());
                au.setCredit(au.getCredit()-propal);
                //Update credit BDD
                um.update(au, au.getNo_utilisateur());
                response.sendRedirect(request.getContextPath()+"/vente/detail?article="+article);
                //Mettre a jour prix vente de l'article.
                avm.updatePrix(article, propal);
            }else{
                //Géré en JS
                response.sendRedirect("/");
            }

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
