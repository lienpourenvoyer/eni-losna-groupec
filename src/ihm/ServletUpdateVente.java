package ihm;

import bll.ArticlesVendusManager;
import bll.EncheresManager;
import bll.RetraitsManager;
import bo.ArticlesVendus;
import bo.Categories;
import bo.Retraits;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "ServletUpdateVente")
public class ServletUpdateVente extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArticlesVendusManager avm = new ArticlesVendusManager();
        ArticlesVendus av = new ArticlesVendus();

        boolean inputUpdate = (request.getParameter("btnModifier") !=null);
        boolean inputDelete = (request.getParameter("btnSupprimer") !=null);

        if(inputUpdate) {
            av.setNo_article(Integer.parseInt(request.getParameter("inputId")));
            av.setNom_article(request.getParameter("inputNom"));
            av.setDescription(request.getParameter("inputDescription"));
            av.setInt_categorie(Integer.parseInt(request.getParameter("inputCategorie")));
            if (!request.getParameter("inputEndDate").isEmpty()) {
                av.setDate_fin_encheres(LocalDate.parse(request.getParameter("inputEndDate")));
            }
            avm.updateVente(av);

            if (!request.getParameter("inputRue").isEmpty() && !request.getParameter("inputVille").isEmpty() && !request.getParameter("inputCP").isEmpty()) {
                RetraitsManager rtm = new RetraitsManager();
                Retraits rt = new Retraits();
                rt.setNo_article(Integer.parseInt(request.getParameter("inputId")));
                rt.setRue(request.getParameter("inputRue"));
                rt.setVille(request.getParameter("inputVille"));
                rt.setCode_postal(request.getParameter("inputCP"));
                rtm.update(rt);
            }

            response.sendRedirect("detail?article="+av.getNo_article()+"&success=true");
        }

        if(inputDelete) {
            avm.deleteVente(Integer.parseInt(request.getParameter("inputId")));
            response.sendRedirect("../index");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
