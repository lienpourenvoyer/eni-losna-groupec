package ihm;

import bll.ArticlesVendusManager;
import bll.EncheresManager;
import bll.RetraitsManager;
import bo.ArticlesVendus;
import bo.Encheres;
import bo.Retraits;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.time.LocalDate;

@WebServlet(name = "ServletUpload")
public class ServletUpload extends HttpServlet {
    // location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "upload";

    // upload settings
    private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> uploadParameters = new ArrayList<>();
        // checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            System.out.println("Form must has enctype");
            return;
        }
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // sets temporary location to store files
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        ServletFileUpload upload = new ServletFileUpload(factory);

        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);

        String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) { uploadDir.mkdir();}

        try {
            // parses the request's content to extract file data
            @SuppressWarnings("unchecked") List<FileItem> formItems = upload.parseRequest(request);
            if (formItems != null && formItems.size() > 0) {
                //TODO mettre l'image "file missing" quand il n'ya pas d'image.
                for (FileItem item : formItems) {
                    if (!item.isFormField()) {
                        String fileName = new File(item.getName()).getName();
                        String filePath = uploadPath + File.separator + fileName;
                        File storeFile = new File(filePath);

                        item.write(storeFile);
                        System.out.println("Uploded good at : " + filePath);
                        uploadParameters.add(UPLOAD_DIRECTORY + File.separator + fileName);
                    }else{
                        //String fieldName  = item.getFieldName();
                        String fieldValue = item.getString();
                        uploadParameters.add(fieldValue);

                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Uploded bad" + ex.getMessage());
        }

        ArticlesVendusManager avm = new ArticlesVendusManager();
        EncheresManager em        = new EncheresManager();
        RetraitsManager rm        = new RetraitsManager();

        ArticlesVendus uploadedArticle = new ArticlesVendus(
                uploadParameters.get(1),
                uploadParameters.get(2),
                LocalDate.parse(uploadParameters.get(5)),
                LocalDate.parse(uploadParameters.get(6)),
                Integer.parseInt(uploadParameters.get(4)),
                Integer.parseInt(uploadParameters.get(10)),
                Integer.parseInt(uploadParameters.get(3)),
                uploadParameters.get(0));

        avm.insertVente(uploadedArticle);
        em.insert(new Encheres(uploadedArticle.getInt_utilisateur(), uploadedArticle.getNo_article(), uploadedArticle.getDate_debut_encheres(), uploadedArticle.getPrix_initial()));
        rm.insert(new Retraits(uploadedArticle.getNo_article(), uploadParameters.get(7), uploadParameters.get(8), uploadParameters.get(9)));
        response.sendRedirect(request.getContextPath() + "/");

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
