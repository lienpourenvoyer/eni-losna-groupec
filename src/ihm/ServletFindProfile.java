package ihm;

import bll.UtilisateursManager;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/ServletFindProfile")
public class ServletFindProfile extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/profileShow.jsp");
        UtilisateursManager um = new UtilisateursManager();
        String idUtil = request.getParameter("vendeur");
        try{
            Utilisateurs vendeur = um.find(idUtil);
            request.setAttribute("vendeur", vendeur);

        } catch (Exception e) {
            e.printStackTrace();
        }
        rd.forward(request,response);
    }
}
