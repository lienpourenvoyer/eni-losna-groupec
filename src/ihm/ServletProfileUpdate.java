package ihm;

import bll.UtilisateursManager;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletProfile")
public class ServletProfileUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
        UtilisateursManager um = new UtilisateursManager();
        Utilisateurs active_user = (Utilisateurs) session.getAttribute("active_user");

        boolean inputUpdate = (request.getParameter("inputUpdate") !=null);
        boolean inputDelete = (request.getParameter("inputDelete") !=null);

        if(inputUpdate) {
            active_user.setPseudo(request.getParameter("inputPseudo"));
            active_user.setNom(request.getParameter("inputNom"));
            active_user.setPrenom(request.getParameter("inputPrenom"));
            active_user.setEmail(request.getParameter("inputMail"));
            active_user.setTelephone(request.getParameter("inputTelephone"));
            active_user.setVille(request.getParameter("inputVille"));
            active_user.setCode_postale(request.getParameter("inputCP"));
            active_user.setRue(request.getParameter("inputRue"));

            if (!request.getParameter("inputCurrentPassword").isEmpty() && !request.getParameter("inputNewPassword").isEmpty()) {
                if (request.getParameter("inputCurrentPassword").equals(active_user.getMot_de_passe())) {
                    if (request.getParameter("inputNewPassword").equals(request.getParameter("inputConfirmationPassword"))) {
                        active_user.setMot_de_passe(request.getParameter("inputNewPassword"));
                    }
                }
            }

            session.setAttribute("active_user", active_user);
            um.update(active_user, active_user.getNo_utilisateur());
            request.setAttribute("success", "Votre compte a bien été modifié !");
            rd = request.getRequestDispatcher("/WEB-INF/profileUpdate.jsp");
            rd.forward(request,response);
        }

        if(inputDelete) {
            session.invalidate();
            um.delete(active_user.getNo_utilisateur());
            response.sendRedirect("../index");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/profileUpdate.jsp");
        rd.forward(request,response);
    }
}
