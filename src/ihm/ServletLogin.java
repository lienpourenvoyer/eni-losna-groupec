package ihm;

import bll.UtilisateursManager;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletLogin")
public class ServletLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //TODO Verifier que le httpSession n'est pas déjà utilisé
        HttpSession session = request.getSession();
        RequestDispatcher rd;
        UtilisateursManager um = new UtilisateursManager();
        Utilisateurs user = new Utilisateurs();
        String login = request.getParameter("inputID");
        String pswd  = request.getParameter("inputPassword");
        if(um.login(login, pswd) == 0){ // pas de compte
            request.setAttribute("none_account", true);
            rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
            rd.forward(request,response);
        }else{
            //session.setAttribute("id_login", um.login(login, pswd));
            user = um.find(""+um.login(login, pswd));
            session.setAttribute("active_user",user);
            response.sendRedirect(request.getContextPath());
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
        rd.forward(request,response);
    }
}
