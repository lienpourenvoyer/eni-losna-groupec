package ihm;

import bll.ArticlesVendusManager;
import bll.CategoriesManager;
import bo.ArticlesVendus;
import bo.Categories;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

@WebServlet(name = "ServletIndex", urlPatterns={"/ServletIndex"})
public class ServletIndex extends HttpServlet {

    public void init() throws ServletException{
        CategoriesManager cm = new CategoriesManager();
        List<Categories> listeCate = cm.afficherLesCategories();

        this.getServletContext().setAttribute("categories", listeCate);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArticlesVendusManager avm = new ArticlesVendusManager();
        HttpSession session = request.getSession();
        Utilisateurs au = (Utilisateurs) session.getAttribute("active_user");

        String nom = request.getParameter("nom");
        String cate = request.getParameter("cate");

        if(nom == null){ nom = "";}
        try {

            List<ArticlesVendus> listeArticles = null;
            if(cate == null && nom.equals(""))
            {
                listeArticles = avm.afficherLesArticles();
            }
            else
            {
                if(cate == null){ cate = "0";}
                else{cate = cate;}
                int cates = Integer.parseInt(cate);
                if(!nom.equals("")){ nom = "%"+nom+"%";}
                listeArticles = avm.afficherArticleByCategorie(cates, nom);
            }
            System.out.println(listeArticles);

            //Filtres pages Index
            Iterator<ArticlesVendus> i = listeArticles.iterator();
            boolean r_ventes = (request.getParameter("r_ventes") != null && request.getParameter("r_ventes").equals("on"));
            boolean v_cours = (request.getParameter("v_cours") != null && request.getParameter("r_ventes").equals("on"));
            boolean v_non = (request.getParameter("v_non") != null && request.getParameter("r_ventes").equals("on"));
            boolean v_terminees = (request.getParameter("v_terminees") != null && request.getParameter("r_ventes").equals("on"));

            boolean r_achats = (request.getParameter("r_achats") != null && request.getParameter("r_achats").equals("on"));
            boolean e_ouvertes = (request.getParameter("e_ouvertes") != null && request.getParameter("e_ouvertes").equals("on"));
            boolean e_cours = (request.getParameter("e_cours") != null && request.getParameter("e_cours").equals("on"));
            boolean e_win = (request.getParameter("e_win") != null && request.getParameter("e_win").equals("on"));

            while(i.hasNext()){
                ArticlesVendus articles = i.next();
                if(r_ventes){ //L'active user est propriétaire de l'article
                    if(articles.getNo_utilisateur().getNo_utilisateur() != au.getNo_utilisateur()){i.remove();}
                    if (v_cours){ // + date de l'enchere soit aprés ajd
                        if(!articles.getDate_fin_encheres().isAfter(LocalDate.now())){i.remove();}
                    }
                    if(v_non){// + date début soit aprés ajd
                        if(!articles.getDate_debut_encheres().isAfter(LocalDate.now())){i.remove();}
                    }
                    if(v_terminees){// + date fin est avant ajd
                        if(!articles.getDate_fin_encheres().isBefore(LocalDate.now())){i.remove();}
                    }
                }
                if(r_achats){//active user n'est pas proprio
                    if(articles.getNo_utilisateur().getNo_utilisateur() == au.getNo_utilisateur()){i.remove();}
                    if (e_ouvertes){//enchere en cour
                        if(!articles.getDate_fin_encheres().isAfter(LocalDate.now())){i.remove();}
                    }
                    if (e_cours){//faire un apl manager voir dans la Table Enchere pour voir id article

                    }
                    if (e_win){
                        if(!articles.getDate_fin_encheres().isBefore(LocalDate.now())){i.remove();}
                    }
                }

            }


            request.setAttribute("listeArticles", listeArticles);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
        rd.forward(request,response);
    }
}
