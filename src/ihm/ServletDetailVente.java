package ihm;

import bll.ArticlesVendusManager;
import bll.EncheresManager;
import bll.RetraitsManager;
import bo.ArticlesVendus;
import bo.Categories;
import bo.Utilisateurs;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ServletDetailVente")
public class ServletDetailVente extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/detailVente.jsp");
        ArticlesVendusManager am = new ArticlesVendusManager();
        ArticlesVendus av = am.selectID(Integer.parseInt(request.getParameter("article")));

        //Lister la catégorie
        List<Categories> categories = (List<Categories>) getServletContext().getAttribute("categories");
        av.setNo_categorie(categories.get(av.getInt_categorie()-1));

        //Information sur les encheres
        EncheresManager em = new EncheresManager();
        List<String> info_auc = em.selectID(av.getNo_article());

        //Verifier si l'enchere est remportée
        boolean isWon = (av.getDate_fin_encheres().isBefore(LocalDate.now()));

        //Si l'acheteur à supprimé son compte
        if(info_auc == null && isWon){
            info_auc = new ArrayList<String>();
            info_auc.add("0");
            info_auc.add("[UTILISATEUR SUPPRIME]");
        }else if(info_auc == null){
            //Pas obligatoire (a voir)
            info_auc = new ArrayList<String>();
            info_auc.add("0");
            info_auc.add("le vendeur");
            av.setPrix_vente(av.getPrix_initial());
            am.updatePrix(av.getNo_article(), av.getPrix_initial());
        }else{
            av.setPrix_vente(Integer.parseInt(info_auc.get(0)));
        }

        request.setAttribute("active_auc", info_auc );
        //Information retrait
        RetraitsManager rm = new RetraitsManager();
        request.setAttribute("info_rt", rm.selectID(av.getNo_article()));


        //TODO if isWon active.user.setCredit(propal) + update avec UtilisateurManager
        request.setAttribute("isWon", isWon);
        request.setAttribute("active_art", av);
        rd.forward(request,response);
    }
}
