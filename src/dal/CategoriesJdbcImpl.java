package dal;

import bo.ArticlesVendus;
import bo.Categories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoriesJdbcImpl implements CategoriesDAO{

    private static final String SELECT_ALL = "SELECT * FROM categories";

    public List<Categories> selectAll() {
        List<Categories> listeCategorie = new ArrayList<Categories>();

        try(Connection cnx = ConnectionProvider.getConnection())
        {
            PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
            ResultSet rs = pstmt.executeQuery();
            Categories categoriesCourant = new Categories();

            while(rs.next())
            {
                categoriesCourant = categoriesBuilder(rs);
                listeCategorie.add(categoriesCourant);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return listeCategorie;
    }

    private Categories categoriesBuilder(ResultSet rs) throws SQLException {
        Categories categoriesCourant =  new Categories();
        categoriesCourant.setNo_categorie(rs.getInt("no_categorie"));
        categoriesCourant.setLibelle(rs.getString("libelle"));
        return categoriesCourant;
    }
}
