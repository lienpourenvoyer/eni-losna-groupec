package dal;

import bo.Retraits;

public interface RetraitsDAO {
    public void insert(Retraits rt);
    public Retraits selectID(int id_article);
    public void update(Retraits rt);
}
