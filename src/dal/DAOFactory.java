package dal;

public abstract class DAOFactory {

    public static CategoriesDAO getCategoriesDAO(){return new CategoriesJdbcImpl();}
    public static UtilisateursDAO getUtilisateursDAO() {return new UtilisateursJdbcImpl();}
    public static ArticlesVendusDAO getArticlesVendusDAO() {return new ArticlesVendusJdbcImpl();}
    public static EncheresDAO getEnchereDAO() {return new EncheresJdbcImpl();}
    public static RetraitsDAO getRetraitsDAO() {return new RetraitsJdbcImpl();}
}
