package dal;

import bo.Encheres;

import java.time.LocalDate;
import java.util.List;

public interface EncheresDAO {
    public void insert(Encheres en);
    public List<String> selectID(int id);
    public void encherir(int id_art, int id_user, int montant, LocalDate date);
}
