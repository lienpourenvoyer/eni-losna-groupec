package dal;

import bo.ArticlesVendus;

import java.util.List;

public interface ArticlesVendusDAO {

    public List<ArticlesVendus> selectAll();
    public void insertVente(ArticlesVendus av);
    public ArticlesVendus selectID(int id);

    public void updateVente(ArticlesVendus av);
    public void updatePrix(int id_art, int prix);

    public List<ArticlesVendus> selectByCategorie(int noCate, String nomArticle);

    public void deleteVente(Integer id);
}
