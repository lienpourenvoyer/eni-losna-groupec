package dal;

import bo.ArticlesVendus;
import bo.Utilisateurs;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticlesVendusJdbcImpl implements ArticlesVendusDAO {

    private static final String SELECT_ALL = "SELECT * FROM articles_vendus a INNER JOIN utilisateurs u ON u.no_utilisateur = a.no_utilisateur ";
    private static final String INSERT = "insert into public.articles_vendus(nom_article, description, date_debut_encheres, date_fin_encheres, prix_initial, prix_vente, no_utilisateur, no_categorie, img) values (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_ID = "SELECT * FROM articles_vendus WHERE no_article = ?;";
    private static final String UPDATE = "UPDATE public.articles_vendus SET nom_article=?, description=?, no_categorie=?, date_fin_encheres=? WHERE no_article= ?;";
    private static final String DELETE = "DELETE FROM public.encheres WHERE no_article = ?; DELETE FROM public.articles_vendus WHERE no_article = ?;";
    private static final String SELECT_BY_CATEGORIE = "SELECT * FROM articles_vendus a INNER JOIN utilisateurs u ON u.no_utilisateur = a.no_utilisateur WHERE no_categorie = ? OR nom_article LIKE ?";
    private static final String UPDATE_PRIX = "UPDATE public.articles_vendus SET prix_vente = ? WHERE no_article = ?;";

    public List<ArticlesVendus> selectAll() {
        List<ArticlesVendus> listeArticles = new ArrayList<ArticlesVendus>();

        try(Connection cnx = ConnectionProvider.getConnection())
        {
            PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL);
            ResultSet rs = pstmt.executeQuery();
            ArticlesVendus articlesVendusCourant = new ArticlesVendus();

            while(rs.next())
            {
                articlesVendusCourant = articlesVendusBuilder(rs);
                listeArticles.add(articlesVendusCourant);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return listeArticles;
    }

    @Override
    public void insertVente(ArticlesVendus av) {
        try(Connection cnx = ConnectionProvider.getConnection())
        {
            PreparedStatement statement = cnx.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            statement.setObject(1, av.getNom_article(), Types.VARCHAR);
            statement.setObject(2, av.getDescription(), Types.VARCHAR);
            statement.setObject(3, av.getDate_debut_encheres(), Types.TIMESTAMP);
            statement.setObject(4, av.getDate_fin_encheres(), Types.TIMESTAMP);
            statement.setObject(5, av.getPrix_initial(), Types.INTEGER);
            statement.setObject(6, av.getPrix_vente(), Types.INTEGER);
            statement.setObject(7, av.getInt_utilisateur(), Types.INTEGER);
            statement.setObject(8, av.getInt_categorie(), Types.INTEGER);
            statement.setObject(9, av.getImg(), Types.VARCHAR);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur d'insertion de l'article.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    av.setNo_article(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Erreur création d'article: Pas de PK.");
                }
            }finally{
                statement.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public ArticlesVendus selectID(int id) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(SELECT_ID);
            statement.setObject(1, id, Types.INTEGER);

            ResultSet rs = statement.executeQuery();
            ArticlesVendus article;
            while(rs.next())
            {
                article = articlesVendusBuilderGeneral(rs);
                return article;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void updateVente(ArticlesVendus av) {
        try (Connection cnx = ConnectionProvider.getConnection()) {
            PreparedStatement statement = cnx.prepareStatement(UPDATE);
            statement.setObject(1, av.getNom_article(), Types.VARCHAR);
            statement.setObject(2, av.getDescription(), Types.VARCHAR);
            statement.setObject(3, av.getInt_categorie(), Types.INTEGER);
            statement.setObject(4, av.getDate_fin_encheres(), Types.TIMESTAMP);
            statement.setObject(5, av.getNo_article(), Types.INTEGER);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur de mise à jour.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePrix(int id_art, int prix) {
        try (Connection cnx = ConnectionProvider.getConnection()) {
            PreparedStatement statement = cnx.prepareStatement(UPDATE_PRIX);

            statement.setObject(1, prix, Types.INTEGER);
            statement.setObject(2, id_art, Types.INTEGER);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur de mise à jour du prix.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ArticlesVendus> selectByCategorie(int noCate, String nomArticle) {
        List<ArticlesVendus> listeArticles = new ArrayList<ArticlesVendus>();

        try(Connection cnx = ConnectionProvider.getConnection())
        {
            PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_CATEGORIE);
            pstmt.setInt(1, noCate);
            pstmt.setString(2, nomArticle);
            ResultSet rs = pstmt.executeQuery();
            ArticlesVendus articlesVendusCourant = new ArticlesVendus();

            while(rs.next())
            {
                articlesVendusCourant = articlesVendusBuilder(rs);
                listeArticles.add(articlesVendusCourant);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return listeArticles;

    }

    @Override
    public void deleteVente(Integer id) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(DELETE);
            statement.setObject(1, id, Types.INTEGER);
            statement.setObject(2, id, Types.INTEGER);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private ArticlesVendus articlesVendusBuilder(ResultSet rs) throws SQLException{
        ArticlesVendus articlesVendusCourant;
        articlesVendusCourant = new ArticlesVendus();
        articlesVendusCourant.setNo_article(rs.getInt("no_article"));
        articlesVendusCourant.setNom_article(rs.getString("nom_article"));
        articlesVendusCourant.setDescription(rs.getString("description"));
        articlesVendusCourant.setDate_debut_encheres(rs.getDate("date_debut_encheres").toLocalDate());
        articlesVendusCourant.setDate_fin_encheres(rs.getDate("date_fin_encheres").toLocalDate());
        articlesVendusCourant.setPrix_initial(rs.getInt("prix_initial"));
        articlesVendusCourant.setPrix_vente(rs.getInt("prix_vente"));

        Utilisateurs utilisateurCourant = new Utilisateurs();
        utilisateurCourant.setNo_utilisateur(rs.getInt("no_utilisateur"));
        utilisateurCourant.setPseudo(rs.getString("pseudo"));

        articlesVendusCourant.setNo_utilisateur(utilisateurCourant);

        articlesVendusCourant.setImg(rs.getString("img"));
        return articlesVendusCourant;
    }

    private ArticlesVendus articlesVendusBuilderGeneral(ResultSet rs) throws SQLException{
        ArticlesVendus articlesVendusCourant;
        articlesVendusCourant = new ArticlesVendus();
        articlesVendusCourant.setNo_article(rs.getInt("no_article"));
        articlesVendusCourant.setNom_article(rs.getString("nom_article"));
        articlesVendusCourant.setDescription(rs.getString("description"));
        articlesVendusCourant.setDate_debut_encheres(rs.getDate("date_debut_encheres").toLocalDate());
        articlesVendusCourant.setDate_fin_encheres(rs.getDate("date_fin_encheres").toLocalDate());
        articlesVendusCourant.setPrix_initial(rs.getInt("prix_initial"));
        articlesVendusCourant.setPrix_vente(rs.getInt("prix_vente"));
        articlesVendusCourant.setImg(rs.getString("img"));
        articlesVendusCourant.setInt_categorie(rs.getInt("no_categorie"));
        articlesVendusCourant.setInt_utilisateur(rs.getInt("no_utilisateur"));

        return articlesVendusCourant;
    }

}