package dal;

import bo.Utilisateurs;


import java.sql.*;


public class UtilisateursJdbcImpl implements UtilisateursDAO {
    private static final String INSERT = "insert into Utilisateurs (\"pseudo\", \"nom\", \"prenom\", \"email\", \"telephone\", \"rue\", \"code_postal\", \"ville\", \"mot_de_passe\", \"credit\", \"administrateur\") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String DELETE = "UPDATE public.encheres SET no_utilisateur = 0 WHERE no_utilisateur = ?; DELETE FROM public.articles_vendus WHERE no_utilisateur = ?; DELETE FROM public.utilisateurs WHERE no_utilisateur = ?;";
    private static final String UPDATE = "UPDATE public.utilisateurs SET pseudo=?, nom=?, prenom=?, email=?, telephone=?, rue=?, code_postal=?, ville=?, mot_de_passe = ?, credit = ? WHERE no_utilisateur = ?;";
    private static final String LOGIN = "select no_utilisateur from public.utilisateurs where pseudo = ? AND mot_de_passe = ?;";
    private static final String FIND = "select * from public.utilisateurs where no_utilisateur = ?;";
    public UtilisateursJdbcImpl() {
    }

    @Override
    public void insert(Utilisateurs user) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            statement.setObject(1, user.getPseudo(), Types.VARCHAR);
            statement.setObject(2, user.getNom(), Types.VARCHAR);
            statement.setObject(3, user.getPrenom(), Types.VARCHAR);
            statement.setObject(4, user.getEmail(), Types.VARCHAR);
            statement.setObject(5, user.getTelephone(), Types.VARCHAR);
            statement.setObject(6, user.getRue(), Types.VARCHAR);
            statement.setObject(7, user.getCode_postale(), Types.VARCHAR);
            statement.setObject(8, user.getVille(), Types.VARCHAR);
            statement.setObject(9, user.getMot_de_passe(), Types.VARCHAR);
            statement.setObject(10, user.getCredit(), Types.INTEGER);
            statement.setObject(11, user.isAdministrateur(), Types.BOOLEAN);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur d'insertion utilisateur.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setNo_utilisateur(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Erreur création utilisateur: Pas de PK.");
                }
            }finally{
                statement.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void update(Utilisateurs user, Integer id) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(UPDATE);
            statement.setObject(1, user.getPseudo(), Types.VARCHAR);
            statement.setObject(2, user.getNom(), Types.VARCHAR);
            statement.setObject(3, user.getPrenom(), Types.VARCHAR);
            statement.setObject(4, user.getEmail(), Types.VARCHAR);
            statement.setObject(5, user.getTelephone(), Types.VARCHAR);
            statement.setObject(6, user.getRue(), Types.VARCHAR);
            statement.setObject(7, user.getCode_postale(), Types.VARCHAR);
            statement.setObject(8, user.getVille(), Types.VARCHAR);
            statement.setObject(9, user.getMot_de_passe(), Types.VARCHAR);
            statement.setObject(10, user.getCredit(), Types.INTEGER);
            statement.setObject(11, id, Types.INTEGER);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur de mise à jour.");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(DELETE);
            statement.setObject(1, id, Types.INTEGER);
            statement.setObject(2, id, Types.INTEGER);
            statement.setObject(3, id, Types.INTEGER);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int login(String id, String pswd) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(LOGIN);
            statement.setObject(1, id, Types.VARCHAR);
            statement.setObject(2, pswd, Types.VARCHAR);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                return rs.getInt("no_utilisateur");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Utilisateurs find(String id) {
        Utilisateurs user = new Utilisateurs();
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(FIND);
            statement.setObject(1, id, Types.INTEGER);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                user.setNo_utilisateur(rs.getInt("no_utilisateur"));
                user.setPseudo(rs.getString("pseudo"));
                user.setNom(rs.getString("nom"));
                user.setPrenom(rs.getString("prenom"));
                user.setEmail(rs.getString("email"));
                user.setTelephone(rs.getString("telephone"));
                user.setRue(rs.getString("rue"));
                user.setCode_postale(rs.getString("code_postal"));
                user.setVille(rs.getString("ville"));
                user.setMot_de_passe(rs.getString("mot_de_passe"));
                user.setCredit(rs.getInt("credit"));
                user.setAdministrateur(rs.getBoolean("administrateur"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

}
