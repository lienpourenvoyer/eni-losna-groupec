package dal;
import bo.Utilisateurs;

public interface UtilisateursDAO {
    public void insert(Utilisateurs user);
    public void update(Utilisateurs user, Integer id);
    public void delete(Integer id);
    public int login(String id, String pswd);
    public Utilisateurs find(String id);
}
