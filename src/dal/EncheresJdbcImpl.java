package dal;

import bo.Encheres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EncheresJdbcImpl implements EncheresDAO{
    private static final String INSERT = "INSERT INTO public.encheres(no_utilisateur, no_article, date_enchere, montant_enchere) VALUES (?, ?, ?, ?);";
    private static final String SELECT_ID = "SELECT montant_enchere, pseudo FROM public.encheres, public.utilisateurs WHERE encheres.no_article = ? AND (encheres.no_utilisateur = utilisateurs.no_utilisateur);";
    private static final String UPDATE = "UPDATE public.encheres SET no_utilisateur=?, date_enchere=?, montant_enchere=? WHERE no_article = ?;";

    public EncheresJdbcImpl() {
    }

    @Override
    public void insert(Encheres en) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(INSERT);

            statement.setObject(1, en.getNo_utilisateur(), Types.INTEGER);
            statement.setObject(2, en.getNo_article(), Types.INTEGER);
            statement.setObject(3, en.getDate_enchere(), Types.TIMESTAMP);
            statement.setObject(4, en.getMontant_enchere(), Types.INTEGER);

            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<String> selectID(int id) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(SELECT_ID);
            statement.setObject(1, id, Types.INTEGER);
            List<String> info = new ArrayList<String>();

            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                info.add(""+rs.getInt("montant_enchere"));
                info.add(rs.getString("pseudo"));
                return info;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void encherir(int id_art, int id_user, int montant, LocalDate date) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(UPDATE);

            statement.setObject(1, id_user, Types.INTEGER);
            statement.setObject(2, date, Types.TIMESTAMP);
            statement.setObject(3, montant, Types.INTEGER);
            statement.setObject(4, id_art, Types.INTEGER);

            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
