package dal;

import bo.ArticlesVendus;
import bo.Retraits;

import java.sql.*;

public class RetraitsJdbcImpl implements RetraitsDAO {
    private static final String INSERT = "INSERT INTO public.retraits(no_article, rue, code_postal, ville) VALUES (?, ?, ?, ?);";
    private static final String SELECT = "SELECT rue, code_postal, ville FROM public.retraits WHERE no_article = ?;";
    private static final String UPDATE = "UPDATE public.retraits SET rue = ?, code_postal = ?, ville = ? WHERE no_article = ?;";

    @Override
    public void insert(Retraits rt) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(INSERT);

            statement.setObject(1, rt.getNo_article(), Types.INTEGER);
            statement.setObject(2, rt.getRue(), Types.VARCHAR);
            statement.setObject(3, rt.getCode_postal(), Types.VARCHAR);
            statement.setObject(4, rt.getVille(), Types.VARCHAR);

            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Retraits selectID(int id_article) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(SELECT);
            statement.setObject(1, id_article, Types.INTEGER);

            ResultSet rs = statement.executeQuery();
            Retraits rt = new Retraits();
            while(rs.next())
            {
                rt.setRue(rs.getString("rue"));
                rt.setCode_postal(rs.getString("code_postal"));
                rt.setVille(rs.getString("ville"));
                return rt;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Retraits rt) {
        try(Connection cnx = ConnectionProvider.getConnection()){
            PreparedStatement statement = cnx.prepareStatement(UPDATE);
            statement.setObject(1, rt.getRue(), Types.VARCHAR);
            statement.setObject(2, rt.getCode_postal(), Types.VARCHAR);
            statement.setObject(3, rt.getVille(), Types.VARCHAR);
            statement.setObject(4, rt.getNo_article(), Types.INTEGER);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Erreur de mise à jour.");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
