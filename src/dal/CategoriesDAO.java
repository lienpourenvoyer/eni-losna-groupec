package dal;

import bo.Categories;

import java.util.List;

public interface CategoriesDAO {
    public List<Categories> selectAll();

}
