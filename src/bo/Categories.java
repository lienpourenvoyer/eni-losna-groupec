package bo;

import java.io.Serializable;

public class Categories implements Serializable {
    private int no_categorie;
    private String libelle;

    public Categories(int no_categorie, String libelle) {
        this.no_categorie = no_categorie;
        this.libelle = libelle;
    }

    public Categories() {}

    public int getNo_categorie() {
        return no_categorie;
    }

    public void setNo_categorie(int no_categorie) {
        this.no_categorie = no_categorie;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Categories{" + "no_categorie=" + no_categorie + ", libelle='" + libelle + '\'' + '}';
    }
}
