package bo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class ArticlesVendus implements Serializable {
    private int no_article;
    private String nom_article;
    private String description;
    private LocalDate date_debut_encheres;
    private LocalDate date_fin_encheres;
    private int prix_initial;
    private int prix_vente;

    private Utilisateurs no_utilisateur;
    private Categories no_categorie;

    private int int_utilisateur;
    private int int_categorie;

    private List<Encheres> listeEncheres;
    private String img;

    public ArticlesVendus(int no_article, String nom_article, String description, LocalDate date_debut_encheres, LocalDate date_fin_encheres, int prix_initial, int prix_vente, Utilisateurs no_utilisateur, Categories no_categorie) {
        this.no_article = no_article;
        this.nom_article = nom_article;
        this.description = description;
        this.date_debut_encheres = date_debut_encheres;
        this.date_fin_encheres = date_fin_encheres;
        this.prix_initial = prix_initial;
        this.prix_vente = prix_vente;
        this.no_utilisateur = no_utilisateur;
        this.no_categorie = no_categorie;
    }

    public ArticlesVendus(String nom_article, String description, LocalDate date_debut_encheres, LocalDate date_fin_encheres, int prix_initial, Utilisateurs no_utilisateur, Categories no_categorie, String img) {
        this.nom_article = nom_article;
        this.description = description;
        this.date_debut_encheres = date_debut_encheres;
        this.date_fin_encheres = date_fin_encheres;
        this.prix_initial = prix_initial;
        this.prix_vente = prix_initial;
        this.no_utilisateur = no_utilisateur;
        this.no_categorie = no_categorie;
        this.img = img;
    }
public ArticlesVendus(String nom_article, String description, LocalDate date_debut_encheres, LocalDate date_fin_encheres, int prix_initial, int int_utilisateur, int int_categorie, String img) {
        this.nom_article = nom_article;
        this.description = description;
        this.date_debut_encheres = date_debut_encheres;
        this.date_fin_encheres = date_fin_encheres;
        this.prix_initial = prix_initial;
        this.prix_vente = prix_initial;
        this.int_utilisateur = int_utilisateur;
        this.int_categorie = int_categorie;
        this.img = img;
    }

    public ArticlesVendus(){
        this.listeEncheres = listeEncheres;
    }

    public int getNo_article() {
        return no_article;
    }

    public void setNo_article(int no_article) {
        this.no_article = no_article;
    }

    public String getNom_article() {
        return nom_article;
    }

    public void setNom_article(String nom_article) {
        this.nom_article = nom_article;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate_debut_encheres() {
        return date_debut_encheres;
    }

    public void setDate_debut_encheres(LocalDate date_debut_encheres) {
        this.date_debut_encheres = date_debut_encheres;
    }

    public LocalDate getDate_fin_encheres() {
        return date_fin_encheres;
    }

    public void setDate_fin_encheres(LocalDate date_fin_encheres) {
        this.date_fin_encheres = date_fin_encheres;
    }

    public int getPrix_initial() {
        return prix_initial;
    }

    public void setPrix_initial(int prix_initial) {
        this.prix_initial = prix_initial;
    }

    public int getPrix_vente() {
        return prix_vente;
    }

    public void setPrix_vente(int prix_vente) {
        this.prix_vente = prix_vente;
    }

    public Utilisateurs getNo_utilisateur() {
        return no_utilisateur;
    }

    public void setNo_utilisateur(Utilisateurs no_utilisateur) {
        this.no_utilisateur = no_utilisateur;
    }

    public Categories getNo_categorie() {
        return no_categorie;
    }

    public void setNo_categorie(Categories no_categorie) {
        this.no_categorie = no_categorie;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getInt_utilisateur() {
        return int_utilisateur;
    }

    public void setInt_utilisateur(int int_utilisateur) {
        this.int_utilisateur = int_utilisateur;
    }

    public int getInt_categorie() {
        return int_categorie;
    }

    public void setInt_categorie(int int_categorie) {
        this.int_categorie = int_categorie;
    }

    @Override
    public String toString() {
        return "ArticlesVendus{" +
                "no_article=" + no_article +
                ", nom_article='" + nom_article + '\'' +
                ", description='" + description + '\'' +
                ", date_debut_encheres=" + date_debut_encheres +
                ", date_fin_encheres=" + date_fin_encheres +
                ", prix_initial=" + prix_initial +
                ", prix_vente=" + prix_vente +
                ", no_utilisateur=" + no_utilisateur +
                ", no_categorie=" + no_categorie +
                ", listeEncheres=" + listeEncheres +
                ", img='" + img + '\'' +
                '}';
    }
}
