<%--
  Created by IntelliJ IDEA.
  User: Val
  Date: 27/04/2020
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Signin</title>
</head>
<body>
<%@include file="style/styleLogin.jspf"%>
<%@include file="style/warning_pswd.jspf"%>
<div class="container">
    <div class="card card-container">
        <img class="profile-img-card" src="https://www.pointcare.com/wp-content/uploads/2018/12/lock-icon.png" alt="" />

        <p id="profile-name" class="profile-name-card"></p>
        <form method="post" class="form-signin" action="<%=request.getContextPath()%>/signin">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="inputPseudo" name="inputPseudo" class="form-control" placeholder="Pseudo" maxlength="20" required autofocus>
            <input type="text" id="inputNom" name="inputNom" class="form-control" placeholder="Nom" maxlength="20" required autofocus>
            <input type="text" id="inputPrenom" name="inputPrenom" class="form-control" placeholder="Prenom" maxlength="20" required autofocus>
            <input type="email" id="inputMail" name="inputMail" class="form-control" placeholder="e-mail" maxlength="20" required autofocus>
            <input type="tel" id="inputTelephone" name="inputTelephone" class="form-control" placeholder="Teléphone" maxlength="15" required autofocus>
            <input type="text" id="inputVille" name="inputVille" class="form-control" placeholder="Ville" maxlength="30" required autofocus>
            <input type="text" id="inputCP" name="inputCP" class="form-control" placeholder="Code postal" maxlength="10" required autofocus>
            <input type="text" id="inputRue" name="inputRue" class="form-control" placeholder="Rue" maxlength="30" required autofocus>
            <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Mot de passe" maxlength="30" required>
            <input type="password" id="inputPasswordConf" name="inputPasswordConf" class="form-control" placeholder="Confirmation" maxlength="30" required>
            <button class="btn btn-lg btn-primary btn-block btn-login" type="submit">Créer</button>
        </form><!-- /form -->
        <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Annuler</button>
        </form>
    </div><!-- /card-container -->
</div><!-- /container -->
</body>
</html>
