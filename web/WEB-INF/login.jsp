<%--
  Created by IntelliJ IDEA.
  User: Val
  Date: 27/04/2020
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>
<%@include file="style/styleLogin.jspf"%>
<%@include file="style/warning_login.jspf"%>
<div class="container">
    <div class="card card-container">
        <img class="profile-img-card" src="${pageContext.request.contextPath}/img/lock.png" alt="" />

        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" method="post" action="<%=request.getContextPath()%>/login">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="inputID" name="inputID" class="form-control" placeholder="Identifiant" required autofocus>
            <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Mot de passe" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Se souvenir de moi
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-login" type="submit">Se connecter</button>
        </form><!-- /form -->
        <form method="get" action="<%=request.getContextPath()%>/signin">
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">S'incrire</button>
        </form>
        <form method="get" action="<%=request.getContextPath()%>/">
            <button class="btn btn-lg btn-primary btn-block btn-back" type="submit">Retour</button>
        </form>
        <a href="#" class="forgot-password">
            mot de passe oublié?
        </a>
    </div><!-- /card-container -->
</div><!-- /container -->
</body>
</html>
