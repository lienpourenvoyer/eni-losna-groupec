<%--
  Created by IntelliJ IDEA.
  User: Val
  Date: 27/04/2020
  Time: 11:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.*, java.text.*"%>
<%@ page import="bo.Categories" %>
<%@ page import="bo.ArticlesVendus" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>LOSNA</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <%@include file="style/style.jspf"%>
  <div class="header">
    <a class="logo">ENI ENCHERE - Groupe C</a>
    <div class="header-right">
      <c:choose>
        <c:when test="${active_user.getNo_utilisateur() != null}">
          <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
            <a class="active" style="cursor: pointer" onclick="this.parentNode.submit();">Enchéres</a>
          </form>
          <form method="get" action="<%=request.getContextPath()%>/profile">
            <a style="cursor: pointer" onclick="this.parentNode.submit();">Mon Profil</a>
          </form>
          <form method="get" action="<%=request.getContextPath()%>/vendre">
            <a style="cursor: pointer" onclick="this.parentNode.submit();">Vendre un article</a>
          </form>
          <form method="get" action="<%=request.getContextPath()%>/logout">
            <a style="cursor: pointer" onclick="this.parentNode.submit();">Déconnexion</a>
          </form>
        </c:when>
        <c:otherwise>
          <form method="get" action="<%=request.getContextPath()%>/login">
            <a style="cursor: pointer" onclick="this.parentNode.submit();">Login</a>
          </form>
        </c:otherwise>
      </c:choose>
    </div>
  </div>

  <div>
    <div class="jumbotron text-center">
      <h1>Liste des enchères</h1>
    </div>
    <c:if test="${active_user.getNo_utilisateur() != null}">
    <form class="d-flex justify-content-around mt-5" action="<%=request.getContextPath()%>/ServletIndex" method="post">
      <div class="form-check">
        <input class="form-check-input" type="radio" name="r_achats" id="r_achats">
        <label class="form-check-label" for="r_achats">
          Achats
        </label>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="e_ouvertes" name="e_ouvertes">
          <label class="custom-control-label" for="e_ouvertes">enchéres ouvertes</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="e_cours" name="e_cours">
          <label class="custom-control-label" for="e_cours">enchéres en cours</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="e_win" name="e_win">
          <label class="custom-control-label" for="e_win">enchéres remportées</label>
        </div>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" name="r_ventes" id="r_ventes">
        <label class="form-check-label" for="r_ventes">
          Ventes
        </label>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="v_cours" name="v_cours">
          <label class="custom-control-label" for="v_cours">mes ventes en cours</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="v_non" name="v_non">
          <label class="custom-control-label" for="v_non">ventes non débutées</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
          <input type="checkbox" class="custom-control-input" id="v_terminees" name="v_terminees">
          <label class="custom-control-label" for="v_terminees">ventes terminées</label>
        </div>
      </div>
      </c:if>
      <div class="w-25">
        <label>Filtres : </label>
          <% List<String> categories = (List<String>) application.getAttribute("categories");	%>
          <select name="cate" style="width: 353px;">
              <option value="0" disabled selected>Catégorie</option>
              <c:forEach begin="0" end="${categories.size()-1}" varStatus="loop">
                  <option value="${loop.index+1}">${categories.get(loop.index).getLibelle()}</option>
              </c:forEach>
          </select>
        <input style="width:300px;" name="nom" type="text" placeholder="Le nom de l'article contient" value="">
        <button type="submit" class="btn btn-outline-secondary">Rechercher</button>
      </div>
    </form>

    <div class="container">
  <%
      List<ArticlesVendus> listeArticles = (List<ArticlesVendus>) request.getAttribute("listeArticles");
      if(listeArticles != null && listeArticles.size()>0){
          for(ArticlesVendus articles : listeArticles){
  %>
      <div class="card flex-row flex-wrap" style="margin:5px 5px; cursor: pointer;" onclick="window.location='<%= request.getContextPath()%>/vente/detail?article=<%=articles.getNo_article()%>'">
        <div class="card-header border-0">
          <img style="border-radius: 5px; height: 150px; width: 150px; background: transparent no-repeat center; background-size:cover;" src="<%=articles.getImg()%>" alt="">
        </div>
        <div class="card-block px-2">
          <h4 class="card-title"><%=articles.getNom_article()%></h4>
          <p class="card-text"><%=articles.getDescription()%></p>
          <!--Mettre à jour le prix aprés enchéres-->
          <p class="card-text">Prix : <%=articles.getPrix_vente()%> points</p>
          <p class="card-text">Fin de l'enchère : <%=articles.getDate_fin_encheres()%></p>
          <p class="card-text">Vendeur : <a class="btn btn-primary" href="<%= request.getContextPath()%>/ServletFindProfile?vendeur=<%= articles.getNo_utilisateur().getNo_utilisateur()%>"><%=articles.getNo_utilisateur().getPseudo()%></a></p>
        </div>
        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
          Prix de départ : <%=articles.getPrix_initial()%>
        </div>
      </div>
      <%
              }
          }
      %>
    </div>
  </div>
  <script>

    $( document ).ready(function() {
      $('#v_cours').attr("disabled", true).prop("checked", false);
      $('#v_non').attr("disabled", true).prop("checked", false);
      $('#v_terminees').attr("disabled", true).prop("checked", false);

      $('#e_ouvertes').attr("disabled", false);
      $('#e_cours').attr("disabled", false);
      $('#e_win').attr("disabled", false);
    });

    $('#r_achats').click(function () {
      $('#v_cours').attr("disabled", true).prop("checked", false);
      $('#v_non').attr("disabled", true).prop("checked", false);
      $('#v_terminees').attr("disabled", true).prop("checked", false);

      $('#e_ouvertes').attr("disabled", false);
      $('#e_cours').attr("disabled", false);
      $('#e_win').attr("disabled", false);
    });

    $('#r_ventes').click(function () {
      $('#e_ouvertes').attr("disabled", true).prop("checked", false);
      $('#e_cours').attr("disabled", true).prop("checked", false);
      $('#e_win').attr("disabled", true).prop("checked", false);

      $('#v_cours').attr("disabled", false);
      $('#v_non').attr("disabled", false);
      $('#v_terminees').attr("disabled", false);
    });
  </script>
  </body>
</html>
