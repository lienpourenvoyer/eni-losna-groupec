<%--
  Created by IntelliJ IDEA.
  User: Nico
  Date: 28/04/2020
  Time: 09:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Profil - Modifier</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
</head>
<body>
    <%@include file="style/style.jspf"%>
    <%@include file="style/styleLogin.jspf"%>
    <%@include file="style/styleMessage.jspf"%>
    <div class="header">
        <a class="logo">ENI ENCHERE - Groupe C</a>
        <div class="header-right">
            <c:choose>
                <c:when test="${active_user.getNo_utilisateur() != null}">
                    <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Enchéres</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/profile">
                        <a class="active" style="cursor: pointer" onclick="this.parentNode.submit();">Mon Profil</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/vendre">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Vendre un article</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/logout">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Déconnexion</a>
                    </form>
                </c:when>
                <c:otherwise>
                    <form method="get" action="<%=request.getContextPath()%>/login">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Login</a>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <% if(request.getAttribute("success") != null) {%>
     <div class="success">
         <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
         <%=request.getAttribute("success")%>
     </div>
    <%}%>

    <div class="container">
        <div class="card card-container" style="max-width:650px;">
            <form method="post" class="form-signin" action="<%=request.getContextPath()%>/profile/update">
                <label for="inputPseudo">Pseudo</label><input class="form-control" type="text" name="inputPseudo" id="inputPseudo" value="${active_user.getPseudo()}">
                <label for="inputPrenom">Prénom</label><input class="form-control" type="text" name="inputPrenom" id="inputPrenom"  value="${active_user.getPrenom()}">
                <label for="inputNom">Nom</label><input class="form-control" type="text" name="inputNom" id="inputNom" value="${active_user.getNom()}">
                <label for="inputMail">Email</label><input class="form-control" type="email" name="inputMail" id="inputMail" value="${active_user.getEmail()}">
                <label for="inputTelephone">Téléphone</label><input class="form-control" type="tel" name="inputTelephone" id="inputTelephone"  value="${active_user.getTelephone()}">
                <label for="inputRue">Rue</label><input class="form-control" type="text" name="inputRue" id="inputRue" value="${active_user.getRue()}">
                <label for="inputVille">Ville</label><input class="form-control" type="text" name="inputVille" id="inputVille" value="${active_user.getVille()}">
                <label for="inputCP">Code postal</label><input class="form-control" type="text" name="inputCP" id="inputCP" value="${active_user.getCode_postale()}">
                <br>
                <label for="inputCurrentPassword">Mot de passe</label>
                <input class="form-control" type="password" name="inputCurrentPassword" id="inputCurrentPassword" placeholder="Mot de passe actuel">
                <input class="form-control" type="password" name="inputNewPassword" id="inputNewPassword" placeholder="Nouveau mot de passe">
                <input class="form-control" type="password" name="inputConfirmationPassword" id="inputConfirmationPassword" placeholder="Confirmation mot de passe">

                <input style="display: none;" type="text" id="activeUser" name="activeUser" class="form-control" value="${active_user.getNo_utilisateur()}">
                <br>
                <a class="btn btn-lg btn-primary btn-block btn-signin" href="<%=request.getContextPath()%>/profile">Annuler</a>
                <button class="btn btn-lg btn-primary btn-block btn-signin" name="inputUpdate" id="inputUpdate" value="inputUpdate" type="submit">Enregistrer</button>
                <button class="btn btn-lg btn-primary btn-block btn-back" name="inputDelete" id="inputDelete" value="inputDelete" type="submit">Supprimer mon compte</button>
            </form><!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>
