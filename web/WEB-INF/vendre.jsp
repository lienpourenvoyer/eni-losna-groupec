<%--
  Created by IntelliJ IDEA.
  User: Val
  Date: 29/04/2020
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.*, java.text.*"%>
<%@ page import="bo.Categories" %>
<%@ page import="bo.ArticlesVendus" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Vendre Article</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<%@include file="style/style.jspf"%>
<%@include file="style/styleLogin.jspf"%>
<div class="header">
    <a class="logo">ENI ENCHERE - Groupe C</a>
    <div class="header-right">
        <c:choose>
            <c:when test="${active_user.getNo_utilisateur() != null}">
                <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
                    <a style="cursor: pointer" onclick="this.parentNode.submit();">Enchéres</a>
                </form>
                <form method="get" action="<%=request.getContextPath()%>/profile">
                    <a style="cursor: pointer" onclick="this.parentNode.submit();">Mon Profil</a>
                </form>
                <form method="get" action="<%=request.getContextPath()%>/vendre">
                    <a class="active" style="cursor: pointer" onclick="this.parentNode.submit();">Vendre un article</a>
                </form>
                <form method="get" action="<%=request.getContextPath()%>/logout">
                    <a style="cursor: pointer" onclick="this.parentNode.submit();">Déconnexion</a>
                </form>
            </c:when>
            <c:otherwise>
                <form method="get" action="<%=request.getContextPath()%>/login">
                    <a style="cursor: pointer" onclick="this.parentNode.submit();">Login</a>
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="container">
    <div class="card card-container">
        <img id="blah" class="profile-img-card" src="${pageContext.request.contextPath}/img/upload.png" alt="" />

        <form method="post" class="form-signin" action="${pageContext.request.contextPath}/upload" enctype="multipart/form-data">
            <label style="padding-left: 20%; margin-bottom: 5px; cursor:pointer;" for="file">Uploader une image</label>
            <br>
            <input class="inputfile" type="file" id="file" onchange="readURL(this);" name="file" accept="image/png, image/jpeg"/>
            <input type="text" id="inputArticle" name="inputArticle" class="form-control" placeholder="Article" required autofocus>
            <input type="text" id="inputDescription" name="inputDescription" class="form-control" placeholder="Description..." required autofocus>
            <% List<String> categories = (List<String>) application.getAttribute("categories");	%>
            <select class="form-control" name="categorie" >
                <option value="0" disabled selected>Catégorie</option>
                <c:forEach begin="0" end="${categories.size()-1}" varStatus="loop">
                    <option value="${loop.index+1}">${categories.get(loop.index).getLibelle()}</option>
                </c:forEach>
            </select>
            <input type="number" id="inputPrix" name="inputPrix" class="form-control" placeholder="Mise à prix" required autofocus>
            <label for="inputStartDate">Début enchère</label><input type="date" id="inputStartDate" name="inputStartDate" class="form-control" required autofocus>
            <label for="inputEndDate">Fin enchère</label><input type="date" id="inputEndDate" name="inputEndDate" class="form-control" required autofocus>
            <br>
            <p class="profile-name-card">Retrait</p>
            <input type="text" id="inputRue" name="inputRue" class="form-control" placeholder="Rue" required autofocus value="${active_user.getRue()}">
            <input type="text" id="inputCP" name="inputCP" class="form-control" placeholder="Code postal" required autofocus value="${active_user.getCode_postale()}">
            <input type="text" id="inputVille" name="inputVille" class="form-control" placeholder="Ville" required autofocus value="${active_user.getVille()}">
            <!--INPUTS MISE EN VENTE-->
            <input style="display: none;" type="text" id="activeUser" name="activeUser" class="form-control" value="${active_user.getNo_utilisateur()}">
            <br>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Créer l'annonce</button>
        </form><!-- /form -->
        <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
            <button class="btn btn-lg btn-primary btn-block btn-back" type="submit">Annuler</button>
        </form>
    </div><!-- /card-container -->
</div><!-- /container -->
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
    $('#blah')
    .attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
    }
    }
</script>
</body>
</html>
