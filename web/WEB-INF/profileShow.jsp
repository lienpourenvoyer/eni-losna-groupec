<%--
  Created by IntelliJ IDEA.
  User: Nico
  Date: 28/04/2020
  Time: 11:02
  To change this template use File | Settings | File Templates.
--%>
<%@page import="bo.Utilisateurs"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Profil</title>
</head>
<body>
    <%@include file="style/style.jspf"%>
    <div class="header">
        <a class="logo">ENI ENCHERE - Groupe C</a>
        <div class="header-right">
            <c:choose>
                <c:when test="${active_user.getNo_utilisateur() != null}">
                    <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Enchéres</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/profile">
                        <a class="active" style="cursor: pointer" onclick="this.parentNode.submit();">Mon Profil</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/vendre">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Vendre un article</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/logout">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Déconnexion</a>
                    </form>
                </c:when>
                <c:otherwise>
                    <form method="get" action="<%=request.getContextPath()%>/login">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Login</a>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <section class="container mt-5">
        <div class="row">
            <c:choose>
                <c:when test="${vendeur.getNo_utilisateur() != null}">
                    <p class="col-6 text-right">Pseudo : </p>
                    <p class="col-6"><strong>${vendeur.getPseudo()}</strong></p>
                    <p class="col-6 text-right">Nom :  </p>
                    <p class="col-6">${vendeur.getNom()}</p>
                    <p class="col-6 text-right">Prénom : </p>
                    <p class="col-6">${vendeur.getPrenom()}</p>
                    <p class="col-6 text-right">Email : </p>
                    <p class="col-6">${vendeur.getEmail()}</p>
                    <p class="col-6 text-right">Téléphone : </p>
                    <p class="col-6">${vendeur.getTelephone()}</p>
                    <p class="col-6 text-right">Rue : </p>
                    <p class="col-6">${vendeur.getRue()}</p>
                    <p class="col-6 text-right">Code Postal : </p>
                    <p class="col-6">${vendeur.getCode_postale()}</p>
                    <p class="col-6 text-right">Ville : </p>
                    <p class="col-6">${vendeur.getVille()}</p>
                </c:when>
                <c:otherwise>
                    <p class="col-6 text-right">Pseudo : </p>
                    <p class="col-6"><strong>${active_user.getPseudo()}</strong></p>
                    <p class="col-6 text-right">Nom :  </p>
                    <p class="col-6">${active_user.getNom()}</p>
                    <p class="col-6 text-right">Prénom : </p>
                    <p class="col-6">${active_user.getPrenom()}</p>
                    <p class="col-6 text-right">Email : </p>
                    <p class="col-6">${active_user.getEmail()}</p>
                    <p class="col-6 text-right">Téléphone : </p>
                    <p class="col-6">${active_user.getTelephone()}</p>
                    <p class="col-6 text-right">Rue : </p>
                    <p class="col-6">${active_user.getRue()}</p>
                    <p class="col-6 text-right">Code Postal : </p>
                    <p class="col-6">${active_user.getCode_postale()}</p>
                    <p class="col-6 text-right">Ville : </p>
                    <p class="col-6">${active_user.getVille()}</p>
                    <p class="col-6 text-right">Crédits : </p>
                    <p class="col-6"><strong>${active_user.getCredit()}</strong></p>
                </c:otherwise>
            </c:choose>
        </div>


        <section class="text-center">
        <c:choose>
            <c:when test="${vendeur.getNo_utilisateur() == null || vendeur.getNo_utilisateur() == active_user.getNo_utilisateur()}">
                <a class="btn btn-outline-secondary ml-5" href="<%=request.getContextPath()%>/profile/update">Modifier</a>
            </c:when>
        </c:choose>
        </section>
    </section>
</body>
</html>
