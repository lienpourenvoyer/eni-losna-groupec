<%--
  Created by IntelliJ IDEA.
  User: Nico
  Date: 04/05/2020
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="bo.Categories" %>
<%@ page import="bo.ArticlesVendus" %>
<%@ page import="java.util.List" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Detail vente</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <%@include file="style/style.jspf"%>
    <%@include file="style/styleLogin.jspf"%>
    <%@include file="style/styleMessage.jspf"%>
    <%@include file="style/warning_vendu.jspf"%>
    <div class="header">
        <a class="logo">ENI ENCHERE - Groupe C</a>
        <div class="header-right">
            <c:choose>
                <c:when test="${active_user.getNo_utilisateur() != null}">
                    <form method="get" action="<%=request.getContextPath()%>/ServletIndex">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Enchéres</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/profile">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Mon Profil</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/vendre">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Vendre un article</a>
                    </form>
                    <form method="get" action="<%=request.getContextPath()%>/logout">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Déconnexion</a>
                    </form>
                </c:when>
                <c:otherwise>
                    <form method="get" action="<%=request.getContextPath()%>/login">
                        <a style="cursor: pointer" onclick="this.parentNode.submit();">Login</a>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <c:set var="loggedOut" value="false"/>
    <c:set var="owner" value="false"/>


    <c:choose>
        <c:when test="${active_user.getNo_utilisateur() == null}">
            <c:set var="loggedOut" value="true"/>
        </c:when>
        <c:when test="${(active_user.getNo_utilisateur() == active_art.getInt_utilisateur())}">
            <c:set var="owner" value="true"/>
        </c:when>
    </c:choose>

    <c:set var='success' value='<%= request.getParameter("success") %>'/>
    <c:if test = "${success}">
        <div class="success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Cette vente à bien été modifiée</strong>
        </div>
    </c:if>

    <div class="container">
        <div class="row">
            <div class="col-md-4 mt-5">
                <img style="border-radius: 5px; height: 300px; width: 300px; background: transparent no-repeat center; background-size:cover;" src="../${active_art.getImg()}"  class="img-thumbnail"><br>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="font-weight-bold">Article en vente - Enchère n°${active_art.getNo_article()}</h3>
                    </div>
                    <c:if test = "${owner}">
                    <form method="post" action="<%=request.getContextPath()%>/vente/update">
                        <input class="form-control" type="hidden" id="inputId" name="inputId" value="${active_art.getNo_article()}" />
                            </c:if>
                        <div class="card-body">
                        <c:if test = "${loggedOut || !owner}">
                            <h3>${active_art.getNom_article()} </h3>
                        </c:if>
                        <c:if test = "${owner}">
                            <label>Nom de l'article : </label>
                            <input class="form-control" type="text" name="inputNom" id="inputNom" value="${active_art.getNom_article()}">
                        </c:if>
                        <div>
                            <label>Description : </label>
                            <c:if test = "${loggedOut || !owner}">
                                <strong>${active_art.getDescription()}</strong>
                            </c:if>
                            <c:if test = "${owner}">
                                <input class="form-control" type="text" name="inputDescription" id="inputDescription" value="${active_art.getDescription()}">
                            </c:if>
                        </div>
                        <div>
                            <label>Catégorie : </label>
                            <c:if test = "${loggedOut || !owner}">
                                <strong>${active_art.getNo_categorie().getLibelle()}</strong>
                            </c:if>
                            <c:if test = "${owner}">
                                <% List<String> categories = (List<String>) application.getAttribute("categories");	%>
                                <select class="form-control" name="inputCategorie" id="inputCategorie">
                                    <c:forEach begin="0" end="${categories.size()-1}" varStatus="loop">
                                        <option value="${loop.index+1}" <c:if test = "${loop.index+1 == active_art.getInt_categorie()}">selected</c:if>>${categories.get(loop.index).getLibelle()}</option>
                                    </c:forEach>
                                </select>
                            </c:if>
                        </div>

                        <div>
                            <c:choose>
                                <c:when test="${isWon}">
                                    <label style="color: red;">Remportée par : </label>
                                    <strong>${active_auc.get(1)} (${active_art.getPrix_vente()} points)</strong>
                                </c:when>
                                <c:otherwise>
                                    <label style="color: red;">Meilleur Offre: </label>
                                    <strong>${active_art.getPrix_vente()} par ${active_auc.get(1)}</strong>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div>
                            <label>Mise à prix : </label>
                            <strong>${active_art.getPrix_initial()}</strong>
                        </div>

                        <div>
                            <label>Fin de l'enchère : </label>
                            <c:if test = "${loggedOut || !owner}">
                                <strong>${active_art.getDate_fin_encheres()}</strong>
                            </c:if>
                            <c:if test = "${owner}">
                                <input class="form-control"  type="date" id="inputEndDate" name="inputEndDate" value="${active_art.getDate_fin_encheres()}">
                            </c:if>
                        </div>

                        <div>
                            <c:if test = "${loggedOut || !owner}">
                                <label>Retrait : </label><strong>${info_rt.getRue()}, ${info_rt.getVille()} (${info_rt.getCode_postal()})</strong>
                            </c:if>
                            <c:if test = "${owner}">
                                <div>
                                    <label>Rue :</label><input type="text" class="form-control" name="inputRue" id="inputRue" value="${info_rt.getRue()}">
                                </div>
                                <div>
                                    <label>Ville :</label><input type="text" class="form-control" name="inputVille" id="inputVille" value="${info_rt.getVille()}">
                                </div>
                                <div>
                                    <label>Code postal :</label><input type="text" class="form-control" name="inputCP" id="inputCP" value="${info_rt.getCode_postal()}">
                                </div>
                            </c:if>
                        </div>

                        <c:if test = "${!owner && !loggedOut}">
                            <form action="<%= request.getContextPath()%>/Enchere?article=${active_art.getNo_article()}" method="post">
                                <div class="form-group">
                                    <label  id="l_propal" for="proposition">Ma proposition: </label>
                                    <input class="form-control" id="proposition" name="proposition" type="number" min="${active_art.getPrix_vente()}" value="${active_art.getPrix_vente()+1}">
                                </div>
                                <div class="form_buttons">
                                    <button id="b_propal" type="submit" class="btn btn-outline-success">Envoyer</button>
                                </div>
                            </form>
                        </c:if><br>
                        <c:if test = "${owner}">
                            <div class="form_buttons">
                                <button type="submit" id="btnModifier" name="btnModifier" class="btn btn-success">Modifier</button>
                                <button type="submit" id="btnSupprimer" name="btnSupprimer" class="btn btn-danger">Supprimer l'enchère</button>
                            </div>
                        </form>
                        </c:if>
                        <strong><a style="margin-top: 5px; padding: 0;" href="<%= request.getContextPath()%>/ServletFindProfile?vendeur=${active_art.getInt_utilisateur()}"  class="nav-link">Profil vendeur</a></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function solde() {
            if($('#proposition').val() > ${active_user.getCredit()}){
                $('#b_propal').html("Points inssufisant");
                $('#b_propal').attr("disabled", true);
            }else{
                $('#b_propal').html("Envoyer");
                $('#b_propal').attr("disabled", false);
            }
        }

        $( document ).ready(function() {
            if(${isWon}){
                $('#proposition').attr("disabled", true);
                $('#btnModifier').attr("disabled", true);
                $('#btnSupprimer').attr("disabled", true);
                $('#btnModifier').html("Modifications impossibles");
                $('#btnSupprimer').html("Suppression impossible");
            }
            solde();
        });

        $('#proposition').change(function () {
            solde();
        })
    </script>
</body>
</html>
